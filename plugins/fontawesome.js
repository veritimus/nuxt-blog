import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faFacebookF,
  faTwitter,
  faTumblr,
  faPinterestP,
  faPinterest,
  faLinkedinIn,
  faDribbble
} from '@fortawesome/free-brands-svg-icons'
import {
  faBars,
  faPen,
  faRss,
  faSearch,
  faTags,
  faUser,
  faStar as faStarSolid,
  faStarHalfAlt,
  faHome,
  faGlobeAmericas,
  faMusic,
  faQuoteLeft,
  faLink,
  faLongArrowAltLeft,
  faLongArrowAltRight,
  faTimes,
  faHeart as faHeartSolid,
  faChevronRight,
  faChevronLeft
} from '@fortawesome/free-solid-svg-icons'
import {
  faCalendarAlt,
  faComments,
  faHeart as faHeartRegular,
  faImage,
  faStar as faStarRegular,
  faEnvelope
} from '@fortawesome/free-regular-svg-icons'

config.autoAddCss = false
library.add(
  faFacebookF,
  faTwitter,
  faTumblr,
  faPinterest,
  faPinterestP,
  faLinkedinIn,
  faDribbble,
  faBars,
  faRss,
  faSearch,
  faUser,
  faCalendarAlt,
  faImage,
  faComments,
  faHeartSolid,
  faHeartRegular,
  faTags,
  faPen,
  faStarSolid,
  faStarRegular,
  faStarHalfAlt,
  faHome,
  faGlobeAmericas,
  faEnvelope,
  faMusic,
  faQuoteLeft,
  faLink,
  faLongArrowAltLeft,
  faLongArrowAltRight,
  faChevronRight,
  faChevronLeft,
  faTimes
)

Vue.component('font-awesome-icon', FontAwesomeIcon)
