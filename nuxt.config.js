
module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Nuxt personal blog',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Personal blog coded with Nuxt' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: 'https://polyfill.io/v3/polyfill.min.js?features=WebAnimations' }
    ]
  },
  loading: { color: '#fff' },
  css: ['@fortawesome/fontawesome-svg-core/styles.css'],
  plugins: [
    '@/plugins/axios.js',
    '@/plugins/fontawesome.js'
  ],
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/moment'
  ],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxtjs/style-resources'
  ],
  axios: {
    baseURL: process.env.API_URL || 'https://portfolio-api.pawelspierewka.pl/api/v1/blog'
  },
  build: {
    styleResources: {
      scss: ['./assets/styles/main.scss']
    },
    extend (config, ctx) {
    }
  }
}
