export const state = () => ({
  asideArticleCategories: [],
  asideArticlesLatest: [],
  isNavigationActive: false
})

export const mutations = {
  setAsideArticleCategories (state, categories) {
    state.asideArticleCategories = categories
  },
  setAsideArticlesLatest (state, categories) {
    state.asideArticlesLatest = categories
  },
  toggleNavigationState (state) {
    state.isNavigationActive = !state.isNavigationActive
  }
}

export const actions = {
  async fetchAsideArticleCategories ({ commit }) {
    const { categories } = await this.$axios.$get('/categories')
    commit('setAsideArticleCategories', categories)
  },
  async fetchAsideArticlesLatest ({ commit }) {
    const { posts } = await this.$axios.$get('/posts?page=1&quantity=4')
    commit('setAsideArticlesLatest', posts)
  },
  async nuxtServerInit ({ dispatch }) {
    await dispatch('fetchAsideArticleCategories')
    await dispatch('fetchAsideArticlesLatest')
  },
  toggleNavigationState ({ commit }) {
    commit('toggleNavigationState')
  }
}
