export const state = () => ({
  list: [],
  liked: [],
  commentsPageIndex: 1,
  commentsPerPage: 5,
  postsPageIndex: 1,
  postsPerPage: 5,
  post: null,
  totalComments: null,
  totalPosts: null
})

export const mutations = {
  addComment (state, comment) {
    state.post.comments.push(comment)
  },
  dislikePost (state, postId) {
    state.list.find(post => post.id === postId).likes -= 1
    state.liked.splice(state.liked.indexOf(postId), 1)
  },
  likePost (state, postId) {
    state.list.find(post => post.id === postId).likes += 1
    state.liked.push(postId)
  },
  setComments (state, comments) {
    state.post.comments = comments
  },
  setCommentsPage (state, page) {
    state.commentsPageIndex = page
  },
  setPost (state, post) {
    state.post = post
  },
  setPosts (state, posts) {
    state.list = posts
  },
  setPostsPage (state, page) {
    state.postsPageIndex = page
  },
  setTotalComments (state, quantity) {
    state.totalComments = quantity
  },
  setTotalPosts (state, quantity) {
    state.totalPosts = quantity
  }
}

export const actions = {
  addComment ({ commit, getters }, payload) {
    const comment = { ...payload, id: getters.getLastCommentId() + 1 }
    commit('addComment', comment)
  },
  async changeCommentsPage ({ commit, dispatch }, page) {
    commit('setCommentsPage', page)
    await dispatch('fetchComments')
  },
  async changePostsPage ({ commit, dispatch }, payload) {
    const { page, query } = payload
    commit('setPostsPage', page)
    if (!query.tag && !query.category) {
      await dispatch('fetchPosts')
    } else {
      await dispatch('fetchPosts', query)
    }
  },
  async fetchComments ({ commit, state }) {
    const { data } = await this.$axios.get(`/posts/${state.post.slug}/comments?page=${state.commentsPageIndex}&quantity=${state.commentsPerPage}`)
    commit('setComments', data.comments)
    commit('setTotalComments', data.totalComments)
  },
  async fetchPost ({ commit }, slug) {
    const { post } = await this.$axios.$get(`/posts/${slug}`)
    commit('setPost', post)
    commit('setTotalComments', post.totalComments)
  },
  async fetchPosts ({ commit, state }, query) {
    if (!query) {
      const { posts, totalPosts } = await this.$axios.$get(`/posts?page=${state.postsPageIndex}&quantity=${state.postsPerPage}`)
      commit('setPosts', posts)
      commit('setTotalPosts', totalPosts)
    } else if (query.tag) {
      const { posts, totalPosts } = await this.$axios.$get(`/posts?tag=${query.tag}&page=${state.postsPageIndex}&quantity=${state.postsPerPage}`)
      commit('setPosts', posts)
      commit('setTotalPosts', totalPosts)
    } else {
      const { posts, totalPosts } = await this.$axios.$get(`/posts?category=${query.category}&page=${state.postsPageIndex}&quantity=${state.postsPerPage}`)
      commit('setPosts', posts)
      commit('setTotalPosts', totalPosts)
    }
  },
  likePost ({ commit, getters }, postId) {
    getters.isPostLiked(postId) ? commit('dislikePost', postId) : commit('likePost', postId)
  }
}

export const getters = {
  getComments: state => () => state.post.comments.length > 0 ? [...state.post.comments].reverse() : [],
  getLastCommentId: state => () => state.post.comments[state.post.comments.length - 1].id,
  getTotalCommentsPages: state => () => Math.ceil(state.totalComments / state.commentsPerPage),
  getTotalPostsPages: state => () => Math.ceil(state.totalPosts / state.postsPerPage),
  isPostLiked: state => postId => state.liked.includes(postId)
}
