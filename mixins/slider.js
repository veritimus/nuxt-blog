export default {
  props: {
    itemSwitchDuration: {
      type: Number,
      default: 500
    },
    itemSwitchEasing: {
      type: String,
      default: 'ease'
    },
    itemSwitchForward: {
      type: Boolean,
      default: true
    },
    itemSwitchTranslationValue: {
      type: Number,
      default: 580
    },
    items: {
      type: Array,
      default: () => []
    },
    itemsSwitchInterval: {
      type: Number,
      default: 5000
    },
    sliderHorizontally: {
      type: Boolean,
      default: true
    }
  },
  data () {
    return {
      activeItemIndex: 0
    }
  },
  methods: {
    getDOMElements () {
      return { slider: this.slider, activeSlideTab: this.activeSlideTab }
    },
    startSlider (DOMElements, translationValue, switchDuration, switchEasing, isHorizontally, isSwitchForward, switchInterval = 5000) {
      setInterval(() => this.switchSlide(DOMElements, translationValue, switchDuration, switchEasing, isHorizontally, isSwitchForward), switchInterval)
    },
    switchSlide ({ slider, activeSlideTab }, translationValue = 200, switchDuration = 1000, switchEasing = 'ease', isHorizontally = true, isSwitchForward = true) {
      const options = { duration: switchDuration, easing: switchEasing, fill: 'forwards' }
      if (isSwitchForward) {
        if (this.activeItemIndex + 1 < this.items.length) {
          this.activeItemIndex += 1
          return slider.animate([
            { transform: `translate${isHorizontally ? 'Y' : 'X'}(-${(this.activeItemIndex - 1) * translationValue}px)` },
            { transform: `translate${isHorizontally ? 'Y' : 'X'}(-${this.activeItemIndex * translationValue}px)` }
          ], options)
        } else {
          this.activeItemIndex = 0
          return slider.animate([
            { transform: `translate${isHorizontally ? 'Y' : 'X'}(-${(this.items.length - 1) * translationValue}px)` },
            { transform: `translate${isHorizontally ? 'Y' : 'X'}(0)` }
          ], options)
        }
      }
      if (this.activeItemIndex === 0) {
        this.activeItemIndex = this.items.length - 1
        slider.animate([
          { transform: `translate${isHorizontally ? 'Y' : 'X'}(0)` },
          { transform: `translate${isHorizontally ? 'Y' : 'X'}(-${this.activeItemIndex * translationValue}px)` }
        ], options)
      } else {
        this.activeItemIndex -= 1
        slider.animate([
          { transform: `translate${isHorizontally ? 'Y' : 'X'}(-${(this.activeItemIndex + 1) * translationValue}px)` },
          { transform: `translate${isHorizontally ? 'Y' : 'X'}(-${this.activeItemIndex * translationValue}px)` }
        ], options)
      }
      if (activeSlideTab) {
        activeSlideTab.classList.toggle('active')
      }
    }
  }
}
